<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templates extends MX_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function defaultTemplate($data = NULL)
  {
    $this->load->view('default_header_view', $data);
    $this->load->view('default_body_view', $data);
    $this->load->view('default_footer_view');
  }

}
