<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <!-- /.Tittle content -->
      <?php echo $contentTittle; ?>
      <!-- /.End Tittlt content -->
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <!-- /.Body content -->
  <?php $this->load->view($contentView); ?>
  <!-- /.End Body content -->
</div>
